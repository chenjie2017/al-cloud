package com.ruoyi.gen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.gen.domain.GeneratorDataSource;
import com.ruoyi.gen.mapper.GeneratorDataSourceMapper;
import com.ruoyi.gen.service.IGeneratorDataSourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 数据源Service业务层处理
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@Service
public class GeneratorDataSourceServiceImpl extends ServiceImpl<GeneratorDataSourceMapper, GeneratorDataSource> implements IGeneratorDataSourceService {
    @Autowired
    private GeneratorDataSourceMapper generatorDataSourceMapper;


    /**
     * 查询数据源列表
     *
     * @param generatorDataSource 数据源
     * @return 数据源
     */
    @Override
    public List<GeneratorDataSource> selectGeneratorDataSourceList(GeneratorDataSource generatorDataSource) {
        return generatorDataSourceMapper.selectGeneratorDataSourceList(generatorDataSource);
    }


}
