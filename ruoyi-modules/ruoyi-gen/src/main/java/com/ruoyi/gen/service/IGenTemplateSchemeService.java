package com.ruoyi.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.gen.domain.GenTemplateScheme;

import java.util.List;

/**
 * 代码生成模板组管理Service接口
 *
 * @author ruoyi
 * @date 2020-12-24
 */
public interface IGenTemplateSchemeService extends IService<GenTemplateScheme> {


    /**
     * 查询代码生成模板组管理列表
     *
     * @param genTemplateScheme 代码生成模板组管理
     * @return 代码生成模板组管理集合
     */
    List<GenTemplateScheme> selectGenTemplateSchemeList(GenTemplateScheme genTemplateScheme);


}
